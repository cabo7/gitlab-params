#!/usr/bin/env python

import sys
import os

def countFilesDir(appFolder):
    totalDir = 0
    totalFiles = 0
    for base, dirs, files in os.walk(appFolder):
        for directories in dirs:
            totalDir = totalDir + 1
        for Files in files:
            totalFiles = totalFiles + 1
    with open('python_output.env', 'w') as f:
        f.write('NUM_OF_FILES={0}'.format(totalFiles))
    f.close()
    
if __name__ == "__main__":
    countFilesDir(sys.argv[1])
    