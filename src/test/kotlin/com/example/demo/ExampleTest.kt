package com.example.demo

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class ExampleTest {
    val hello = Hello()

    @Test
    fun `Assert hello prints Hello World`(){
        Assertions.assertEquals(hello.printHello(), "Hello World")
    }
}